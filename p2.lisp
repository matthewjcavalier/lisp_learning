(defun calc-fib (val1 val2 end total)
  (let ((summed (+ val1 val2)))
    (cond ((< summed end)
	   (setq total (+ total (if (= (mod summed 2) 0) summed 0)))
	   (calc-fib val2 summed end total))
	  (t total)))
  )
  

(defun get-fib-less-than (ceil)
  (write (calc-fib 0 1 ceil 0 ))
  )
