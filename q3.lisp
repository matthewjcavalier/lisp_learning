(ql:quickload :iterate)
(use-package :iterate)


					; The prime factors of 13195 are 5, 7, 13 and 29.
					;
					; What is the largest prime factor of the number 600851475143 ?


(defun is-prime (primes-list val)
  (if primes-list
      (if (/= (mod val (car primes-list)) 0)
	  (is-prime (cdr primes-list) val))
      t))


(defun counter (num)
  (let ((primes-list nil))
    (iter (for i from 2 to num)
	  (if (is-prime primes-list i)
	      (setq primes-list (append primes-list i))))
    (write (car (reverse primes-list)))))

(defun prepend (val list)
  (reverse (append val (reverse list))))
  
