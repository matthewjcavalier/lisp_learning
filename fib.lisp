(ql:quickload :iterate)
(use-package :iterate)

(defun get-value (val)
  "If input values is divisble by either 3 or 5 returns the value else zero is returned"
  (if (or (= (mod val 3) 0) (= (mod val 5) 0))
      val
      0))

(defun fib (ceiling)
  (let ((total 0))
    (iter (for i from 1 to (- ceiling 1))
	  (setq total (+ total (get-value i))))
    (write total)))
